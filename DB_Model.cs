﻿/*
 * @author Erik Carrillo
 * @email hell-dxd@hotmail.com
 * @email devs@kllmp.org
 * @version 1.0.0.1
 * 
 **/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace MSSQL
{
    namespace Models
    {
        public class DB_Model
        {
            protected SqlDataReader MSSQL_Registro;

            protected string MSSQL_StrConexion { set => _strConexion = value; }
            protected bool MSSSQL_StatuError { get =>  _StatuError; }
            protected string MSSQL_MesnajeError { get; set; }

            private bool _StatuError = false;
            private SqlConnection Con = null;
            private string _strConexion = "";

            

            protected void MSSQL_Open()
            {
                try
                {
                    Con = new SqlConnection(_strConexion);
                    _StatuError = false;
                    Con.Open();
                }
                catch (Exception error)
                {
                    MSSQL_MesnajeError = error.Message;
                    _StatuError = true;
                }
            }

            protected void MSSQL_Close()
            {
                try
                {
                    Con.Close();
                    MSSQL_Registro.Close();
                    _StatuError = false;
                }
                catch (Exception err)
                {
                    _StatuError = true;
                    MSSQL_MesnajeError = err.Message;
                }
            }

            protected void MSSQL_Exec(string query)
            {
                try
                {
                    if (_StatuError || Con == null) { return; }
                    SqlCommand cmd = new SqlCommand(query, Con);
                    cmd.CommandType = CommandType.Text;
                    MSSQL_Registro = cmd.ExecuteReader();
                    _StatuError = false;
                }
                catch (Exception error)
                {
                    _StatuError = true;
                    MSSQL_MesnajeError = error.Message;
                }
            }

            protected void MSSQL_Exec(string storedProcedure, List<SqlParameter> parametros)
            {
                try
                {
                    if (_StatuError || Con == null) { return; }
                    
                    SqlCommand cmd = new SqlCommand(storedProcedure, Con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (parametros != null)
                    {
                        foreach (var parametreo in parametros)
                        {
                            cmd.Parameters.Add(parametreo);
                        }
                    }

                    MSSQL_Registro = cmd.ExecuteReader();
                    _StatuError = false;
                }
                catch (Exception error)
                {
                    _StatuError = true;
                    MSSQL_MesnajeError = error.Message;
                }
            }

            protected bool MSSQL_Fetch()
            {
                bool status = false;
                try
                {
                    if (!_StatuError && MSSQL_Registro.HasRows)
                    {
                        if (MSSQL_Registro.Read())
                        {
                            status = true;
                        }
                    }
                }
                catch (Exception err)
                {
                    _StatuError = true;
                    MSSQL_MesnajeError = err.Message;
                }

                return status;
            }
        }
    }

    namespace Libreria
    {
        public class Conexion
        {
            public string StrConexion { set => _strConexion = value; }
            public bool StatuError { get => _StatuError; }
            public string MesnajeError { get; set; }
            public SqlDataReader Registro { get => _Registro; }

            private bool _StatuError = false;
            private SqlConnection Con = null;
            private string _strConexion = "";
            private SqlDataReader _Registro;


            public void Open()
            {
                try
                {
                    Con = new SqlConnection(_strConexion);
                    _StatuError = false;
                    Con.Open();
                }
                catch (Exception error)
                {
                    MesnajeError = error.Message;
                    _StatuError = true;
                }
            }

            public void Close()
            {
                try
                {
                    Con.Close();
                    Registro.Close();
                    _StatuError = false;
                }
                catch (Exception err)
                {
                    _StatuError = true;
                    MesnajeError = err.Message;
                }
            }

            public void Exec(string query)
            {
                try
                {
                    if (_StatuError || Con == null) { return; }
                    SqlCommand cmd = new SqlCommand(query, Con);
                    cmd.CommandType = CommandType.Text;
                    _Registro = cmd.ExecuteReader();
                    _StatuError = false;
                }
                catch (Exception error)
                {
                    _StatuError = true;
                    MesnajeError = error.Message;
                }
            }

            public void Exec(string storedProcedure, List<SqlParameter> parametros)
            {
                try
                {
                    if (_StatuError || Con == null) { return; }

                    SqlCommand cmd = new SqlCommand(storedProcedure, Con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (parametros != null)
                    {
                        foreach (var parametreo in parametros)
                        {
                            cmd.Parameters.Add(parametreo);
                        }
                    }

                    _Registro = cmd.ExecuteReader();
                    _StatuError = false;
                }
                catch (Exception error)
                {
                    _StatuError = true;
                    MesnajeError = error.Message;
                }
            }

            public bool Fetch()
            {
                bool status = false;
                try
                {
                    if (!_StatuError && _Registro.HasRows)
                    {
                        if (_Registro.Read())
                        {
                            status = true;
                        }
                    }
                }
                catch (Exception err)
                {
                    _StatuError = true;
                    MesnajeError = err.Message;
                }

                return status;
            }
        }
    }
}