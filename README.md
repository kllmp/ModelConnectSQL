# DB_Model

_Clase Modelo para conectar a base de datos SQL Server_

## Uso
Su utilidad esta en sus dos opciones la primera como una instancia y la segunda una clase base que se hereda al modelo.
```
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using dbModel = MSSQL.Models;
using dbLibreria = MSSQL.Libreria;

namespace ProyectoWeb.Business
{
	public class EjemploInstancia
	{
		public int Id {get;set;}
    	public string Titulo {get;set;}
    	public string Descripcion {get;set;}

    	public List<EjemploInstancia> GetElemento()
        {
        	var modelo = new List<EjemploInstancia>();

        	try
        	{
				var db = new dbLibreria.Libreria.Conexion();
				db.StrConexion = ConfigurationManager.ConnectionStrings["Conexion"].ConnectionString;

				db.Open();
				db.Exec("SELECT id, titulo, descripcion FROM tabla");

				while(db.Fetch())
				{
					modelo.Add(new EjemploModelo(){
	                	Id = (int)db.Registro[0],
	                	Titulo = (string)db.Registro[1],
	                	Descripcion = (string)db.Registro[2],
	                });
				}
				db.Close();
			}
			catch(Exception)
			{}
			return modelo;
        }

        public List<EjemploInstancia> GetElementoStoredProcedure()
        {
        	var modelo = new List<EjemploInstancia>();

        	try
        	{
				var db = new dbLibreria.Libreria.Conexion();
				db.StrConexion = ConfigurationManager.ConnectionStrings["Conexion"].ConnectionString;

				db.Open();
				db.Exec("[dbo].[NombreStoreProcedure]", new List<System.Data.SqlClient.SqlParameter>() {
	                new System.Data.SqlClient.SqlParameter("@id",System.Data.SqlDbType.Int){ Value = 1, Direction = ParameterDirection.Input }
	            });

				while(db.Fetch())
				{
					modelo.Add(new EjemploModelo(){
	                	Id = (int)db.Registro[0],
	                	Titulo = (string)db.Registro[1],
	                	Descripcion = (string)db.Registro[2],
	                });
				}
				db.Close();
			}
			catch(Exception)
			{}
			return modelo;
        }



	}

    public class EjemploModelo : dbModel.DB_Model
    {

    	public int Id {get;set;}
    	public string Titulo {get;set;}
    	public string Descripcion {get;set;}

        public List<EjemploModelo> GetElemento()
        {
        	var modelo = new List<EjemploModelo>();
        	try
        	{
	        	MSSQL_StrConexion = ConfigurationManager.ConnectionStrings["Conexion"].ConnectionString;
	            MSSQL_Open();
	            MSSQML_Exec("SELECT id, titulo, descripcion FROM tabla");

	            while (MSSQML_Fetch())
	            {
	            	modelo.Add(new EjemploModelo(){
	                	Id = (int)MSSQL_Registro[0],
	                	Titulo = (string)MSSQL_Registro[1],
	                	Descripcion = (string)MSSQL_Registro[2],
	                });
	            }
	            
	            MSSQL_Close();
        	}
        	catch(Exception)
        	{
        	}
        	return modelo;
        }

        public List<EjemploModelo> GetElementoStoreProcedure()
        {
        	var modelo = new List<EjemploModelo>();
        	try
        	{
        		MSSQL_StrConexion = ConfigurationManager.ConnectionStrings["Conexion"].ConnectionString;

	            MSSQL_Open();

	            MSSQML_Exec("[dbo].[NombreStoreProcedure]", new List<System.Data.SqlClient.SqlParameter>() {
	                new System.Data.SqlClient.SqlParameter("@id",System.Data.SqlDbType.Int){ Value = 1, Direction = ParameterDirection.Input }
	            });


	            while (MSSQML_Fetch())
	            {
	                modelo.Add(new EjemploModelo(){
	                	Id = (int)MSSQL_Registro[0],
	                	Titulo = (string)MSSQL_Registro[1],
	                	Descripcion = (string)MSSQL_Registro[2],
	                });
	            }

	            MSSQL_Close();
        	}
        	catch(Exception)
        	{
        	}
        	return modelo;
        }
    }
}
```



